# Ionic 4 Translate

## Setup

### Generate Ionic project

```
ionic start ionic-translate-app blank --type=angular
cd ionic-translate-app
```

### Install the translate library

```
npm install --save @ngx-translate/core @ngx-translate/http-loader
```

### Install the storage library (user selected language)

```
npm install --save @ionic/storage
```

## Setup App

First, Modify `app.module.ts`

Import the module

```
// Translate
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader} from '@ngx-translate/core';
import { TranslateHttpLoader} from '@ngx-translate/http-loader';

// Storage
import { IonicStorageModule } from '@ionic/storage';
```

Need to create factory method to load translation file
```
// AoT requires an exported function for factories
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}
```

Add Storage and Translate module in imports
```
    HttpClientModule,
    IonicStorageModule.forRoot(),
    TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: (createTranslateLoader),
            deps: [HttpClient]
        }
    }),
```

Create translation file

```
mkdir -p src/assets/i18n
touch src/assets/i18n/fr.json
touch src/assets/i18n/en.json
```

## Setup Service

Generate the language service

```
ionic g service services/language
```

Here is the code for the language service
```
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';

const LNG_KEY = 'SELECTED_LANGUAGE';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  availableLanguages: any[] = [
    { value: 'en', text: 'English'},
    { value: 'fr', text: 'French'}
  ];
  selected: string = null;

  constructor(
    private translate: TranslateService,
    private storage: Storage
  ) {}

  getAvailableLanguages() {
    return this.availableLanguages;
  }

  setLanguage(lng) {
    this.translate.use(lng);
    this.selected = lng;
    this.storage.set(LNG_KEY, lng);
  }

  setInitialAppLanguage() {
    let language = this.translate.getBrowserLang();
    this.translate.setDefaultLang(language);

    this.storage.get(LNG_KEY).then(val => {
      if (val) {
        this.setLanguage(val);
        this.selected = val;
      }
    });
  }
}
```

In `app.module.ts`, Add the service in the providers section

```

```

Then, in `app.component.ts`, call `setInitialAppLanguage()`

```
import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { LanguageService } from './services/language.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private languageService: LanguageService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.languageService.setInitialAppLanguage();
    });
  }
}`
```

## Usage (Service, Pipe and Directive)

Add the translate module in `home.module.ts`

```
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({</ul>


  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ]),
    TranslateModule
  ],
  declarations: [HomePage]
})
export class HomePageModule {}
```

With pipe
```
<ion-title>{{ 'HOME.title' | translate }}</ion-title>
```

With pipe and parameters
```
<ion-text color="primary">{{ 'HOME.greeting' | translate:params }}</ion-text>
```

 With directive
 ```
<ion-text color="secondary" [translate]="'HOME.gender'" [translateParams]="params"></ion-text>
```

With directive and html
```
<ion-text color="secondary" [innerHTML]="'HOME.withcode' | translate"></ion-text>
```

